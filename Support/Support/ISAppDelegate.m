//
//  ISAppDelegate.m
//  Support
//
//  Created by Сергей Костян on 12.05.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import "ISAppDelegate.h"

@implementation ISAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    [[ISSupport defaultManager] setServer:@"http://readmeapp.com"];
    [[ISSupport defaultManager] setHeaderTextColor:[UIColor whiteColor]];
    [[ISSupport defaultManager] setDelegate:self];
    
    UINavigationController *n = [[UINavigationController alloc] initWithRootViewController:[[ISSupport defaultManager] ticketVC]];
    [n.navigationBar setBarTintColor:[UIColor redColor]];
    [n.navigationBar setTintColor:[UIColor whiteColor]];
    self.window.rootViewController = n;
    
    return YES;
}

-(UIBarButtonItem *)viewForAddButton
{
    return  [[UIBarButtonItem alloc] initWithTitle:@"Add"
                                             style:UIBarButtonItemStylePlain
                                            target:nil
                                            action:nil];
}

-(UIBarButtonItem *)viewForBackButtonInTicketMessagesView
{
    return  [[UIBarButtonItem alloc] initWithTitle:@"123"
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
