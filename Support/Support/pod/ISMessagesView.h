//
//  ISMessagesView.h
//  Support
//
//  Created by Сергей Костян on 13.05.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PHFComposeBarView.h>
#import "ISSupportHeaders.h"

@class ISTicketMessageCell;

@protocol ISTicketMessageCellDelegate;

@protocol ISTicketMessageCellDelegate <NSObject>

-(void)cell:(ISTicketMessageCell *)cell didPressedCloseButton:(UIButton *)btn;

@end

@interface ISMessagesView : UIViewController <PHFComposeBarViewDelegate, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, ISTicketMessageCellDelegate, UIAlertViewDelegate>
{
    PHFComposeBarView *_bar;
    
    NSMutableArray *_messages;
    NSDateFormatter *_formatter;
     NSDateFormatter *_formatterCurrent;
    
    int _counter;
}

@property (strong) IBOutlet UITableView *tableView;

@property (strong) ISTicket *currentTicket;

@end

@interface ISTicketMessageCell : UITableViewCell

@property (weak) id<ISTicketMessageCellDelegate> delegate;
@property (strong) IBOutlet UILabel *from;
@property (strong) IBOutlet UILabel *date;
@property (strong) IBOutlet UILabel *message;
@property (strong) IBOutlet UIImageView *icon;
@property (strong) IBOutlet UIButton *close;

-(IBAction)closeButtonPressed:(id)sender;

@end
