//
//  ISTicket.m
//  Support
//
//  Created by Сергей Костян on 12.05.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import "ISTicket.h"

@implementation ISTicket

-(id)initWithDictionary:(NSDictionary *)object
{
    if([object isKindOfClass:[NSDictionary class]])
    {
        if(![object[@"id"] isKindOfClass:[NSNull class]])
            _ticketID = [object[@"id"] stringValue];
        if([object[@"title"] isKindOfClass:[NSString class]])
            _title = object[@"title"];
        if([object[@"status"] isKindOfClass:[NSString class]])
            _status = object[@"status"];
        if([object[@"messages_count"] isKindOfClass:[NSString class]])
            _messagesCount = object[@"messages_count"];
    }
    return self;
}

@end
