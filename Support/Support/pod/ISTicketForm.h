//
//  ISTicketForm.h
//  Support
//
//  Created by Сергей Костян on 12.05.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISSupportHeaders.h"

@class ISInputCell;

@interface ISTicketForm : UIViewController <UITextViewDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>
{
    ISInputCell *_subjectCell;
    ISInputCell *_inputCell;
}
@property (strong) NSString *subject;

@property (strong) IBOutlet UITableView *tableView;

@end

@interface ISInputCell : UITableViewCell

@property (strong) IBOutlet UITextView *input;

@end
