//
//  ISTicketForm.m
//  Support
//
//  Created by Сергей Костян on 12.05.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import "ISTicketForm.h"

#define kNewTicketEndpoint @"/api/ticket/new"

@implementation ISInputCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

@interface ISTicketForm ()

@end

@implementation ISTicketForm

#pragma mark - status bar -

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return [ISSupport statusBarStyle];
}

#pragma mark - view -

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    UILabel *titleView = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., 320., 40.)];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.textColor = [[ISSupport defaultManager] headerTextColor];
    titleView.text = [ISSupport alertTitle];
    titleView.font = [[ISSupport defaultManager] headerFont];
    [titleView sizeToFit];
    
    self.navigationItem.titleView = titleView;
    
    UIBarButtonItem *s = ([ISSupport viewForSendButtonInNewTicketView]) ? [ISSupport viewForSendButtonInNewTicketView] : [[UIBarButtonItem alloc] initWithTitle:[ISSupport newTicketSendButtonTitle]
                                                                                                                                                           style:UIBarButtonItemStylePlain
                                                                                                                                                          target:self
                                                                                                                                                          action:nil];
    if(s.customView != nil && [s.customView isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)s.customView;
        [btn addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [s setTarget:self];
        [s setAction:@selector(send)];
    }
    self.navigationItem.rightBarButtonItem = s;
    
    if([ISSupport viewForBackButtonInNewTicketView])
    {
        UIBarButtonItem *backButton = [ISSupport viewForBackButtonInNewTicketView];
        if(backButton.customView != nil && [backButton.customView isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)backButton.customView;
            [btn addTarget:self action:@selector(pop) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.leftBarButtonItem = backButton;
        }
    }
    
    _inputCell = [[[NSBundle mainBundle] loadNibNamed:@"ISInputCell" owner:nil options:nil] lastObject];

    _inputCell.input.delegate = self;
    
    _subjectCell = [[[NSBundle mainBundle] loadNibNamed:@"ISInputCell" owner:nil options:nil] lastObject];
    
    _subjectCell.input.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide)];
    tap.delegate = self;
    [self.tableView addGestureRecognizer:tap];
    // Do any additional setup after loading the view from its nib.
}

-(void)pop
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - keyboard notifications handle -

-(void)keyboardWillShow:(NSNotification *)info
{
    double animationDuration;
    CGRect rect = [[[info userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    animationDuration = [[[info userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve animationType = [[[info userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    UIViewAnimationOptions animation = UIViewAnimationOptionCurveLinear;
    
    CGFloat kbSize = (IS_PORTRAIT) ? rect.size.height : rect.size.width;
    
    void (^completition)(void) = ^{
        _inputCell.frameHeight = _inputCell.frameHeight - kbSize;
	};
    
    if(IS_IOS6_OR_EARLIER)
    {
        switch (animationType) {
            case UIViewAnimationCurveEaseIn:
                animation = UIViewAnimationOptionCurveEaseIn;
                break;
            case UIViewAnimationCurveEaseInOut:
                animation = UIViewAnimationOptionCurveEaseInOut;
                break;
            case UIViewAnimationCurveEaseOut:
                animation = UIViewAnimationOptionCurveEaseOut;
                break;
            case UIViewAnimationCurveLinear:
                animation = UIViewAnimationOptionCurveLinear;
                break;
        }
        [UIView animateWithDuration:animationDuration
                              delay:0.
                            options:animation
                         animations:completition
                         completion:nil];
    }
    else if(IS_IOS7)
    {
        [UIView animateWithDuration:0.5
							  delay:0
			 usingSpringWithDamping:500.0f
			  initialSpringVelocity:0.0f
							options:UIViewAnimationOptionCurveLinear
						 animations:completition
						 completion:nil];
    }
}

-(void)keyboardWillHide:(NSNotification *)info
{
    double animationDuration;
    animationDuration = [[[info userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve animationType = [[[info userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    UIViewAnimationOptions animation = UIViewAnimationOptionCurveLinear;
    
    void (^completition)(void) = ^{
        _inputCell.frameHeight = self.tableView.frameHeight - [self.tableView sectionHeaderHeight]*2 - 160. - [self.tableView sectionFooterHeight]*2;
    };
    
    if(IS_IOS6_OR_EARLIER)
    {
        switch (animationType) {
            case UIViewAnimationCurveEaseIn:
                animation = UIViewAnimationOptionCurveEaseIn;
                break;
            case UIViewAnimationCurveEaseInOut:
                animation = UIViewAnimationOptionCurveEaseInOut;
                break;
            case UIViewAnimationCurveEaseOut:
                animation = UIViewAnimationOptionCurveEaseOut;
                break;
            case UIViewAnimationCurveLinear:
                animation = UIViewAnimationOptionCurveLinear;
                break;
        }
        [UIView animateWithDuration:animationDuration
                              delay:0.
                            options:animation
                         animations:completition
                         completion:nil];
    }
    else if(IS_IOS7)
    {
        [UIView animateWithDuration:0.5
							  delay:0
			 usingSpringWithDamping:500.0f
			  initialSpringVelocity:0.0f
							options:UIViewAnimationOptionCurveLinear
						 animations:completition
						 completion:nil];
    }
}

#pragma mark - touches handle -

-(void)hide
{
    [_subjectCell.input resignFirstResponder];
    [_inputCell.input resignFirstResponder];
}

#pragma mark - send action -

-(void)send
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if([_subjectCell.input.text length] > 0 && ![[_subjectCell.input.text stringByTrimmingCharactersInSet:set] length] == 0)
    {
        if([_inputCell.input.text length] > 0 && ![[_inputCell.input.text stringByTrimmingCharactersInSet:set] length] == 0)
        {
            NSString* URLString = [NSString stringWithFormat:@"%@%@",[ISSupport defaultManager].server,kNewTicketEndpoint];
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setValue:_subjectCell.input.text forKeyPath:@"title"];
            [params setValue:_inputCell.input.text forKeyPath:@"message"];
            if([ISSupport defaultManager].deviceToken)
                [params setValue:[ISSupport defaultManager].deviceToken forKey:@"device_id"];
            
#warning test id
            [params setValue:@"1" forKey:@"device_id"];
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.responseSerializer = [AFJSONResponseSerializer serializer];
            
            [SVProgressHUD showWithStatus:nil maskType:SVProgressHUDMaskTypeGradient];
            [manager POST:URLString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 [SVProgressHUD dismiss];
                 //              if([[responseObject valueForKey:@"status"] isEqualToString:@"ok"])
                 //              {
                 [self.navigationController popViewControllerAnimated:YES];
                 //              }
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 [SVProgressHUD showErrorWithStatus:error.localizedDescription];
             }];
        }
        else
        {
            [_inputCell.input becomeFirstResponder];
        }
    }
    else
    {
        [_subjectCell.input becomeFirstResponder];
    }
}

#pragma mark - table view delegate -

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return [NSString stringWithFormat:@"%@:",[ISSupport newTicketSubjectTitle]];
    return nil;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2.;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
        return 44.;
    return self.tableView.frameHeight - [self.tableView sectionHeaderHeight]*2 - 160. - [self.tableView sectionFooterHeight]*2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1.;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        return _subjectCell;
    }
    if(indexPath.section == 1)
    {
        return _inputCell;

    }
    return nil;
}

@end
