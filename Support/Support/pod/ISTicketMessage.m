//
//  ISMessage.m
//  Support
//
//  Created by Сергей Костян on 13.05.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import "ISTicketMessage.h"

@implementation ISTicketMessage

-(id)initWithDictionary:(NSDictionary *)object
{
    _ticketID = @"";
    _messageID = @"";
    _text = @"";
    _fromUser = @"";
    _createdAt = @"";
    
    if([object isKindOfClass:[NSDictionary class]])
    {
        if(![object[@"id"] isKindOfClass:[NSNull class]])
            _messageID = [object[@"id"] stringValue];
        if(![object[@"ticket_id"] isKindOfClass:[NSNull class]])
            _ticketID = [object[@"ticket_id"] stringValue];
        if([object[@"text"] isKindOfClass:[NSString class]])
            _text = object[@"text"];
        if(![object[@"from_user"] isKindOfClass:[NSNull class]])
            _fromUser = [object[@"from_user"] stringValue];
        if([object[@"created_at"] isKindOfClass:[NSString class]])
            _createdAt = object[@"created_at"];
    }
    return self;
}

@end
