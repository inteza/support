//
//  ISTicketVC.m
//  Support
//
//  Created by Сергей Костян on 12.05.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import "ISTicketsVC.h"
#import "ISSupportHeaders.h"

//#import "ISTicketsCell.h"

#define kCell @"cell"

#define kListEndpoint   @"/api/ticket/list"
#define kDeleteEndpoint @"/api/ticket/delete"

@implementation ISTicketsCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

@interface ISTicketsVC ()

@end

@implementation ISTicketsVC

#pragma mark - status bar -

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return [ISSupport statusBarStyle];
}

#pragma mark - download request -

-(void)_downloadTicketsList
{
    NSString* URLString = [NSString stringWithFormat:@"%@%@",[ISSupport defaultManager].server,kListEndpoint];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if([ISSupport defaultManager].deviceToken)
        [params setValue:[ISSupport defaultManager].deviceToken forKey:@"device_id"];
    
#warning test id
    [params setValue:@"1" forKey:@"device_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [SVProgressHUD showWithStatus:nil maskType:SVProgressHUDMaskTypeGradient];
    [manager POST:URLString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [_resfreshControl endRefreshing];
         [SVProgressHUD dismiss];
         if([[responseObject valueForKey:@"status"] isEqualToString:@"ok"])
         {
             NSArray *tickets = [responseObject valueForKey:@"content"];
             NSMutableArray *receivedTickets = [NSMutableArray array];
             if(tickets.count)
             {
                 for(NSDictionary *ticket in tickets)
                 {
                     ISTicket *t = [[ISTicket alloc] initWithDictionary:ticket];
                     if(t)
                         [receivedTickets addObject:t];
                 }
                 _tickets = receivedTickets;
             }
         }
         [self.tableView reloadData];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [_resfreshControl endRefreshing];
         [SVProgressHUD showErrorWithStatus:error.localizedDescription];
     }];
}

-(void)downloadTicketsList
{
    NSString* URLString = [NSString stringWithFormat:@"%@%@",[ISSupport defaultManager].server,kListEndpoint];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if([ISSupport defaultManager].deviceToken)
        [params setValue:[ISSupport defaultManager].deviceToken forKey:@"device_id"];
    
#warning test id
    [params setValue:@"1" forKey:@"device_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:URLString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [_resfreshControl endRefreshing];
         if([[responseObject valueForKey:@"status"] isEqualToString:@"ok"])
         {
             NSArray *tickets = [responseObject valueForKey:@"content"];
             NSMutableArray *receivedTickets = [NSMutableArray array];
             if(tickets.count)
             {
                 for(NSDictionary *ticket in tickets)
                 {
                     ISTicket *t = [[ISTicket alloc] initWithDictionary:ticket];
                     if(t)
                         [receivedTickets addObject:t];
                 }
                 _tickets = receivedTickets;
             }
         }
         [self.tableView reloadData];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [_resfreshControl endRefreshing];
         [SVProgressHUD showErrorWithStatus:error.localizedDescription];
     }];
}

#pragma mark - view -

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _resfreshControl = [[UIRefreshControl alloc] init];
    [_resfreshControl addTarget:self action:@selector(downloadTicketsList) forControlEvents:UIControlEventValueChanged];

    [self.tableView addSubview:_resfreshControl];
    
    UILabel *titleView = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., 320., 40.)];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.textColor = [[ISSupport defaultManager] headerTextColor];
    titleView.text = [ISSupport ticketsListHeader];
    titleView.font = [[ISSupport defaultManager] headerFont];
    [titleView sizeToFit];
    
    self.navigationItem.titleView = titleView;

    UIBarButtonItem *b = ([ISSupport viewForAddButton]) ? [ISSupport viewForAddButton] : [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                                                                       target:self
                                                                                                                                       action:nil];
    if(b.customView != nil && [b.customView isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)b.customView;
        [btn addTarget:self action:@selector(addNewTicket:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [b setTarget:self];
        [b setAction:@selector(addNewTicket:)];
    }
    
    self.navigationItem.rightBarButtonItem = b;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source -

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tickets.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ISTicketsCell *cell = [tableView dequeueReusableCellWithIdentifier:kCell];
    
    if(!cell)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ISTIcketsCell" owner:self options:nil] lastObject];
    }
    
    ISTicket *ticketInfo = _tickets[indexPath.row];
    
    cell.ticketTitle.text = ticketInfo.title;
    cell.ticketStatus.text = ticketInfo.status;
    cell.ticketMessagesCount.text = ticketInfo.messagesCount;
    if([ticketInfo.status isEqualToString:@"waiting"])
    {
        cell.ticketStatusImage.image = [[UIImage imageNamed:@"is_sinchronize"] changeColor:[UIColor lightGrayColor]];
    }
    if([ticketInfo.status isEqualToString:@"accepted"])
    {
        cell.ticketStatusImage.image = [[UIImage imageNamed:@"is_checkmark"] changeColor:[UIColor greenColor]];
    }
    if([ticketInfo.status isEqualToString:@"declined"])
    {
        cell.ticketStatusImage.image = [[UIImage imageNamed:@"is_close"] changeColor:[UIColor redColor]];
    }
    if([ticketInfo.status isEqualToString:@"active"])
    {
        cell.ticketStatusImage.image = [[UIImage imageNamed:@"is_settings"] changeColor:[UIColor lightGrayColor]];
    }
    // Configure the cell...
    
    return cell;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *a = [_tickets mutableCopy];
    [self deleteTicket:a[indexPath.row]];
    [a removeObjectAtIndex:indexPath.row];
    _tickets = a;
    
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    ISTicket *ticketInfo = _tickets[indexPath.row];
    ISMessagesView *m = [[ISMessagesView alloc] initWithNibName:@"ISMessagesView" bundle:nil];
    m.currentTicket = ticketInfo;
    
    if(![ISSupport viewForBackButtonInTicketMessagesView])
    {
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[ISSupport ticketMessagesBackButtonTitle]
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:nil
                                                                      action:nil];
        self.navigationItem.backBarButtonItem = backButton;
    }
    else if(![[ISSupport viewForBackButtonInTicketMessagesView] customView])
    {
        UIBarButtonItem *backButton = [ISSupport viewForBackButtonInTicketMessagesView];
        self.navigationItem.backBarButtonItem = backButton;
    }
    
    [self.navigationController pushViewController:m animated:YES];
}

#pragma mark - add button -

-(void)addNewTicket:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[ISSupport alertTitle]
                                                    message:[ISSupport alertMessage]
                                                   delegate:self
                                          cancelButtonTitle:[ISSupport alertCancelButtonTitle]
                                          otherButtonTitles:nil];
    NSSet *set = [[ISSupport defaultManager] subjects];
    for(NSString *title in set.allObjects)
        [alert addButtonWithTitle:title];
    [alert show];
}

#pragma mark - alertView delegate -

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex != alertView.cancelButtonIndex)
    {
        NSSet *set = [[ISSupport defaultManager] subjects];
        NSString *subject = [set.allObjects objectAtIndex:buttonIndex-1];
        NSLog(@"%@",subject);
        
        [self createTicketFormWithSubject:subject];
    }
}

#pragma mark - new ticker view -

-(void)createTicketFormWithSubject:(NSString *)subject
{
    if(![ISSupport viewForBackButtonInNewTicketView])
    {
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[ISSupport newTicketBackButtonTitle]
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:nil
                                                                       action:nil];
        self.navigationItem.backBarButtonItem = backButton;
    }
    else if(![[ISSupport viewForBackButtonInNewTicketView] customView])
    {
        UIBarButtonItem *backButton = [ISSupport viewForBackButtonInNewTicketView];
        self.navigationItem.backBarButtonItem = backButton;
    }
    
    ISTicketForm *form = [[ISTicketForm alloc] initWithNibName:@"ISTicketForm" bundle:nil];
    form.subject = subject;
    [self.navigationController pushViewController:form animated:YES];
}

#pragma mark - delete ticket action -

-(void)deleteTicket:(ISTicket *)ticket
{
    NSString* URLString = [NSString stringWithFormat:@"%@%@",[ISSupport defaultManager].server,kDeleteEndpoint];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if([ISSupport defaultManager].deviceToken)
        [params setValue:[ISSupport defaultManager].deviceToken forKey:@"device_id"];
    if(ticket.ticketID)
        [params setValue:ticket.ticketID forKeyPath:@"ticket_id"];
#warning test id
    [params setValue:@"1" forKey:@"device_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:URLString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
    
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
     }];
}

@end
